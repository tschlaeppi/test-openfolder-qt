#include <iostream>

#include <QCoreApplication>
#include <QGuiApplication>
#include <QFileDialog>
#include <QString>
#include <QLabel>
#include <QDebug>

#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{

    QLabel *platformLabel = new QLabel(this);
    platformLabel->setGeometry(QRect(QPoint(10, 10), QSize(200, 20)));
    platformLabel->setText("PlatformName: " + QGuiApplication::platformName());
    platformLabel->setAlignment(Qt::AlignBottom | Qt::AlignRight);

    QLabel *QPAPluginEnvLabel = new QLabel(this);
    QPAPluginEnvLabel->setGeometry(QRect(QPoint(240, 10), QSize(300, 20)));
    QPAPluginEnvLabel->setText("QT_QPA_PLATFORMTHEME: " + qgetenv("QT_QPA_PLATFORMTHEME"));
    QPAPluginEnvLabel->setAlignment(Qt::AlignBottom | Qt::AlignRight);

    QString version = qVersion();
    QLabel *VersionLabel = new QLabel(this);
    VersionLabel->setGeometry(QRect(QPoint(240, 30), QSize(300, 20)));
    VersionLabel->setText("QT Version: " + version);
    VersionLabel->setAlignment(Qt::AlignBottom | Qt::AlignRight);

    m_button = new QPushButton("Write to console", this);
    m_button->setGeometry(QRect(QPoint(10, 100), QSize(200, 50)));
    connect(m_button, SIGNAL(released()), this, SLOT(handleButton()));

    open_file_button = new QPushButton("Open File", this);
    open_file_button->setGeometry(QRect(QPoint(240, 100), QSize(200, 50)));
    connect(open_file_button, SIGNAL(released()), this, SLOT(openFile()));

    open_folder_button = new QPushButton("Open Folder", this);
    open_folder_button->setGeometry(QRect(QPoint(470, 100), QSize(200, 50)));
    connect(open_folder_button, SIGNAL(released()), this, SLOT(openFolder()));

    open_folder_button = new QPushButton("Open Folder (native)", this);
    open_folder_button->setGeometry(QRect(QPoint(10, 180), QSize(200, 50)));
    connect(open_folder_button, SIGNAL(released()), this, SLOT(openBuiltinFolder()));

    open_folder_button = new QPushButton("Open Folder (existing)", this);
    open_folder_button->setGeometry(QRect(QPoint(240, 180), QSize(200, 50)));
    connect(open_folder_button, SIGNAL(released()), this, SLOT(openExistingDirectory()));
}

void MainWindow::handleButton()
{
    // change the text
    std::cout << "writing something to console" << std::endl;
}

void MainWindow::openFile()
{
    std::cout << "openFile: Entering function" << std::endl;
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open file"));
    std::cout << "openFile: Filename is: '" << fileName.toStdString() << "'" << std::endl;
    std::cout << "openFile: Ending function" << std::endl;
}

void MainWindow::openFolder()
{
    std::cout << "openFolder: Entering function" << std::endl;
    QFileDialog dialog;
    dialog.setFileMode(QFileDialog::Directory);
    dialog.setOption(QFileDialog::ShowDirsOnly, true);
    dialog.exec();
    QString folderName = dialog.directory().absolutePath();
    std::cout << "openFolder: Foldername is: '" << folderName.toStdString() << "'" << std::endl;
    std::cout << "openFolder: Ending function" << std::endl;
}

void MainWindow::openExistingDirectory()
{
    std::cout << "openExistingDirectory: Entering function" << std::endl;
    QString folderName = QFileDialog::getExistingDirectory(this, tr("Open Directory"), "/home", QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    std::cout << "openExistingDirectory: Foldername is: '" << folderName.toStdString() << "'" << std::endl;
    std::cout << "openExistingDirectory: Ending function" << std::endl;
}

void MainWindow::openBuiltinFolder()
{
    std::cout << "openBuiltinFolder: Entering function" << std::endl;
    QFileDialog dialog;
    dialog.setFileMode(QFileDialog::Directory);
    dialog.setOption(QFileDialog::ShowDirsOnly, true);
    dialog.setOption(QFileDialog::DontUseNativeDialog, true);
    dialog.exec();
    QString folderName = dialog.directory().absolutePath();
    std::cout << "openBuiltinFolder: Foldername is: '" << folderName.toStdString() << "'" << std::endl;
    std::cout << "openBuiltinFolder: Ending function" << std::endl;
}
#!/bin/bash

export QT_LOGGING_RULES='*.debug=true' 
#export G_MESSAGES_DEBUG='all'

printenv | sort

# execute binary
exec $SNAP/usr/bin/openfolder "$@"
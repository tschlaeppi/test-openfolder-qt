#include "mainwindow.h"
#include <QApplication>
#include <QDebug>

int main(int argc, char *argv[])
{

    
    qDebug() << "QTVersion is: " << qVersion();
    
    
    
    QApplication app(argc, argv);
    MainWindow mainWindow;
    mainWindow.resize(680, 300);
    mainWindow.show();
    return app.exec();
}

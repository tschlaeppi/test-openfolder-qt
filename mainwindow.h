#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>

namespace Ui {
   class MainWindow;
}

class MainWindow : public QMainWindow
{
   Q_OBJECT
public:
   explicit MainWindow(QWidget *parent = 0);
private slots:
   void handleButton();
   void openFile();
   void openFolder();
   void openExistingDirectory();
   void openBuiltinFolder();
private:
   QPushButton *m_button;
   QPushButton *open_file_button;
   QPushButton *open_folder_button;
};

#endif // MAINWINDOW_H